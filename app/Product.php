<?php

namespace App;

use App\Traits\BelongsToBusiness;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use BelongsToBusiness;

    protected $guarded = [];

	protected static function boot () {
		parent::boot();

		static::deleting(function ($product) {
			$product->histories->each->delete();
		});
	}

    public function add(int $quantity)
    {
        if ($quantity < 1) {
            throw new \Exception("Invalid quantity reached add product method");
        }
        $this->quantity = $this->quantity + $quantity;
        $this->save();
    }

    public function take(int $quantity)
    {
        if ($quantity > $this->quantity) {
            throw new \Exception("Invalid quantity reached take product method");
        }
        $this->quantity = $this->quantity - $quantity;
        $this->save();
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

	public function histories()
	{
		return $this->hasMany(History::class);
	}
}
