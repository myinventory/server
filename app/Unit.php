<?php

namespace App;

use App\Traits\BelongsToBusiness;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use BelongsToBusiness;

    protected $guarded = [];

    protected static function boot () {
        parent::boot();

        static::deleting(function ($unit) {
            $unit->products->each()->delete();
        });
    }
}
