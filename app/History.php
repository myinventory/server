<?php

namespace App;

use App\Traits\BelongsToBusiness;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use BelongsToBusiness;

    static $ADD = "ADD";
    static $TAKE = "TAKE";

    protected $guarded = [];

    protected static function boot () {
        parent::boot();

        static::created(function ($history) {
            switch ($history->action) {
                case History::$ADD:
                    $history->product->add($history->quantity);
                    break;
                case History::$TAKE:
                    $history->product->take($history->quantity);
                    break;
            }
        });
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

	public function taker()
	{
		return $this->belongsTo(Taker::class);
	}
}
