<?php

namespace App;

use App\Traits\BelongsToBusiness;
use Illuminate\Database\Eloquent\Model;

class Taker extends Model
{
    use BelongsToBusiness;

    protected $guarded = [];
}
