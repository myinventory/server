<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "quantity" => $this->quantity,
            "minimum_quantity" => $this->minimum_quantity,
            "unit_id" => $this->unit_id,
            "unit" => $this->unit->name,
            "status" => $this->quantity <= $this->minimum_quantity ? "LOW_INVENTORY" : "OK",
            "comment" => $this->comment
        ];
    }
}
