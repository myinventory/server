<?php

namespace App\Http\Controllers;

use App\History;
use App\Http\Resources\HistoryResource;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
		$data = \request()->validate([
			"filter" => [
				"nullable",
				Rule::in([
					"ALL",
					"TAKE",
					"ADD"
				])
			]
		]);

        return HistoryResource::collection(History::orderBy("created_at", "DESC")
			->when(($data["filter"] ?? null) === "ADD", function ($query) use ($data) {
				$query->where("action", "ADD");
			})
			->when(($data["filter"] ?? null) === "TAKE", function ($query) use ($data) {
				$query->where("action", "TAKE");
			})
			->with([
				"product",
				"taker"
			])
			->paginate(10));
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return HistoryResource
	 * @throws \Exception
	 */
    public function store(Request $request)
    {
        $data = \request()->validate([
            "quantity" => [
                "required",
                "numeric",
                "min:0"
            ],
            "action" => [
                "required",
                Rule::in([
                    History::$ADD,
                    History::$TAKE
                ])
            ],
            "product_id" => [
                "required",
                "numeric",
                "min:0",
                Rule::exists('products', "id")->where(function ($query) {
                    return $query->where('business_id', auth()->user()->business_id);
                })
            ],
            "taker_id" => [
                "nullable",
                "numeric",
                "min:0",
                Rule::exists('takers', "id")->where(function ($query) {
                    return $query->where('business_id', auth()->user()->business_id);
                })
            ],
            "comment" => [
                "nullable"
            ]
        ]);

        switch ($data["action"]) {
            case History::$ADD:
                \request()->validate([
                    "quantity" => [
                        "required",
                        "numeric",
                        "min:1"
                    ]
                ]);
                break;
            case History::$TAKE:
                $product = Product::find($data["product_id"]);
                $max = $product->quantity;
                \request()->validate([
                    "quantity" => [
                        "required",
                        "numeric",
                        "max:$max",
                        "min:1"
                    ]
                ]);
                break;
        }

        try {
            DB::beginTransaction();
            $history = History::create($data);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return new HistoryResource($history);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\History  $history
     * @return HistoryResource
     */
    public function show(History $history)
    {
        return new HistoryResource($history);
    }
}
