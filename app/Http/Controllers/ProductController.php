<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $data = \request()->validate([
            "search" => [
                "nullable"
            ],
			"filter" => [
				"nullable",
				Rule::in([
					"ALL",
					"LOW_INVENTORY"
				])
			]
        ]);

        return ProductResource::collection(Product::when($data["search"] ?? null, function ($query) use ($data) {
            $search = $data["search"];
            $query->where("name", "ilike", "%$search%");
        })->when(($data["filter"] ?? null) === "LOW_INVENTORY", function ($query) use ($data) {
			$query->where("quantity", "<=", DB::raw("minimum_quantity"));
		})->orderBy("name")->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ProductResource
     */
    public function store(Request $request)
    {
        $data = \request()->validate([
        "name" => [
            "required",
            Rule::unique('products', "name")->where(function ($query) {
                return $query->where('business_id', auth()->user()->business_id);
            })
        ],
        "quantity" => [
            "required",
            "numeric",
            "min:0"
        ],
        "minimum_quantity" => [
            "nullable",
            "numeric",
            "min:0"
        ],
        "unit_id" => [
            "required",
            "numeric",
            "min:0",
            Rule::exists('units', "id")->where(function ($query) {
                return $query->where('business_id', auth()->user()->business_id);
            })
        ],
        "comment" => [
            "nullable"
        ]
    ]);

        return new ProductResource(Product::create($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return ProductResource
     */
    public function show(Product $product)
    {
        return new ProductResource($product);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return ProductResource
     */
    public function update(Request $request, Product $product)
    {
         $data = \request()->validate([
        "name" => [
            "required",
            Rule::unique('products', "name")->where(function ($query) {
                return $query->where('business_id', auth()->user()->business_id);
            })->ignore($product->id)
        ],
        "minimum_quantity" => [
            "nullable",
            "numeric",
            "min:0"
        ],
        "unit_id" => [
            "required",
            "numeric",
            "min:0",
            Rule::exists('units', "id")->where(function ($query) {
                return $query->where('business_id', auth()->user()->business_id);
            })
        ],
        "comment" => [
            "nullable"
        ]
    ]);

        $product->update($data);

        return new ProductResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return response("", 204);
    }
}
