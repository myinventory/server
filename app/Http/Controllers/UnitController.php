<?php

namespace App\Http\Controllers;

use App\Http\Resources\UnitResource;
use App\Unit;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return UnitResource::collection(Unit::paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return UnitResource
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            "name" => [
                "required",
                Rule::unique('units', "name")->where(function ($query) {
                    return $query->where('business_id', auth()->user()->business_id);
                })
            ]
        ]);

        return new UnitResource(Unit::create($data));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit  $unit
     * @return UnitResource
     */
    public function update(Request $request, Unit $unit)
    {
        $data = $request->validate([
            "name" => [
                "required",
                Rule::unique('units', "name")->where(function ($query) {
                    return $query->where('business_id', auth()->user()->business_id);
                })->ignore($unit->id)
            ]
        ]);

        $unit->update($data);

        return new UnitResource($unit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit)
    {
        $unit->delete();

        return response("", 204);
    }
}
