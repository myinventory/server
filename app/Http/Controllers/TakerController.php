<?php

namespace App\Http\Controllers;

use App\Http\Resources\TakerResource;
use App\Taker;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TakerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
		return TakerResource::collection(Taker::paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return TakerResource
     */
    public function store(Request $request)
    {
		$data = $request->validate([
			"name" => [
				"required",
				Rule::unique('takers', "name")->where(function ($query) {
					return $query->where('business_id', auth()->user()->business_id);
				})
			]
		]);

		return new TakerResource(Taker::create($data));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Taker  $taker
     * @return TakerResource
     */
    public function update(Request $request, Taker $taker)
    {
		$data = $request->validate([
			"name" => [
				"required",
				Rule::unique('takers', "name")->where(function ($query) {
					return $query->where('business_id', auth()->user()->business_id);
				})->ignore($taker->id)
			]
		]);

		$taker->update($data);

		return new TakerResource($taker);
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Taker  $taker
	 * @return \Illuminate\Http\Response
	 * @throws \Exception
	 */
    public function destroy(Taker $taker)
    {
		$taker->delete();

		return response("", 204);
    }
}
