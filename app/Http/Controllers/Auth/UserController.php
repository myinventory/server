<?php

namespace App\Http\Controllers\Auth;

use App\Business;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function store()
    {
        $data = \request()->validate([
            "email" => [
                "required",
                "email"
            ],
            "password" => [
                "required",
                "min:8",
                "max:255"
            ]
        ]);

        $business = Business::create();

        $user = new User();

        $user->email = strtolower($data["email"]);

        $user->password = bcrypt($data["password"]);

        $user->business_id = $business->id;

        $user->save();

        return response([
            "token" => $user->createToken('MyInventory')->accessToken
        ], 201);
    }

    public function me()
    {
        return auth()->user();
    }

	public function logout()
	{
		return [
			"revoked" => request()->user()->token()->revoke()
		];
	}
}
