<?php


namespace App\Traits;
use Illuminate\Database\Eloquent\Builder;

trait BelongsToBusiness
{
    protected static function bootBelongsToBusiness()
    {
        if (auth()->check()) {
            static::creating(function ($model) {
                $model->business_id = auth()->id();
            });

            static::addGlobalScope('business_id', function (Builder $builder) {
                $builder->where('business_id', auth()->id());
            });
        }
    }
}
