<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
	Route::post("/logout", "Auth\UserController@logout");

    Route::apiResource('units', 'UnitController');
    Route::apiResource('takers', 'TakerController');
    Route::apiResource('products', 'ProductController');
    Route::resource('histories', 'HistoryController')->only([
        'index', 'store', 'destroy'
    ]);
    Route::prefix('auth')->group(function () {
        Route::get('me', 'Auth\UserController@me');
    });
});

Route::prefix('auth')->group(function () {
    Route::post('users', 'Auth\UserController@store');
    Route::post('users', 'Auth\UserController@store');
});
