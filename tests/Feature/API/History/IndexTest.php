<?php

namespace Tests\Feature\API\History;

use App\History;
use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCaseUser;

class IndexTest extends TestCaseUser
{
    /** @test */
    public function it_can_be_indexed()
    {
        $product = factory(Product::class)->create();

        $history = factory(History::class, 10)->create([
            "product_id" => $product->id
        ]);

        $response = $this->getJson("/api/histories");

        $this->assertCount(10, $response->json('data'));
    }
}
