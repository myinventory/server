<?php

namespace Tests\Feature\API\History;

use App\Product;
use App\Taker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCaseUser;

class CreateTest extends TestCaseUser
{
    /** @test */
    public function it_can_be_created_to_add()
    {
        $this->withoutExceptionHandling();

        $product = factory(Product::class)->create([
            "quantity" => 5
        ]);

        $taker = factory(Taker::class)->create();

        $response = $this->postJson("/api/histories", [
            "product_id" => $product->id,
            "taker_id" => $taker->id,
            "action" => "ADD",
            "quantity" => 5
        ]);

        $response->assertStatus(201);

        $this->assertDatabaseHas("histories", [
            "product_id" => $product->id,
            "taker_id" => $taker->id,
            "action" => "ADD",
            "quantity" => 5
        ]);

        $this->assertDatabaseHas("products", [
            "id" => $product->id,
            "quantity" => 10
        ]);
    }

    /** @test */
    public function it_can_be_created_to_remove()
    {
        $this->withoutExceptionHandling();

        $product = factory(Product::class)->create([
            "quantity" => 5
        ]);

        $taker = factory(Taker::class)->create();

        $response = $this->postJson("/api/histories", [
            "product_id" => $product->id,
            "taker_id" => $taker->id,
            "action" => "TAKE",
            "quantity" => 5
        ]);

        $response->assertStatus(201);

        $this->assertDatabaseHas("histories", [
            "product_id" => $product->id,
            "taker_id" => $taker->id,
            "action" => "TAKE",
            "quantity" => 5
        ]);

        $this->assertDatabaseHas("products", [
            "id" => $product->id,
            "quantity" => 0
        ]);
    }

    /** @test */
    public function it_fails_taking_too_much()
    {
        $product = factory(Product::class)->create([
            "quantity" => 5
        ]);

        $taker = factory(Taker::class)->create();

        $response = $this->postJson("/api/histories", [
            "product_id" => $product->id,
            "taker_id" => $taker->id,
            "action" => "TAKE",
            "quantity" => 6
        ]);

        $response->assertStatus(422);

        $this->assertDatabaseMissing("histories", [
            "product_id" => $product->id,
            "taker_id" => $taker->id,
            "action" => "TAKE",
            "quantity" => 6
        ]);

        $this->assertDatabaseHas("products", [
            "id" => $product->id,
            "quantity" => 5
        ]);
    }

    /** @test */
    public function it_fails_adding_too_little()
    {
        $product = factory(Product::class)->create([
            "quantity" => 5
        ]);

        $taker = factory(Taker::class)->create();

        $response = $this->postJson("/api/histories", [
            "product_id" => $product->id,
            "taker_id" => $taker->id,
            "action" => "ADD",
            "quantity" => 0
        ]);

        $response->assertStatus(422);

        $this->assertDatabaseMissing("histories", [
            "product_id" => $product->id,
            "taker_id" => $taker->id,
            "action" => "ADD",
            "quantity" => 0
        ]);

        $this->assertDatabaseHas("products", [
            "id" => $product->id,
            "quantity" => 5
        ]);
    }
}
