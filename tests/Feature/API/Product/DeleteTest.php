<?php

namespace Tests\Feature\API\Product;

use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCaseUser;

class DeleteTest extends TestCaseUser
{
    /** @test */
    public function it_can_be_deleted()
    {
        $product = factory(Product::class)->create();

        $response = $this->deleteJson("/api/products/$product->id");

        $response->assertStatus(204);

        $this->assertDatabaseMissing("products", [
            "id" => $product->id
        ]);
    }
}
