<?php

namespace Tests\Feature\API\Product;

use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCaseUser;

class IndexTest extends TestCaseUser
{
    /** @test */
    public function it_can_be_indexed()
    {
        $products = factory(Product::class, 10)->create();

        $response = $this->getJson("/api/products");

        $this->assertCount(10, $response->json('data'));
    }
}
