<?php

namespace Tests\Feature\API\Product;

use App\Unit;
use Tests\TestCaseUser;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateTest extends TestCaseUser
{
   /** @test */
   public function it_can_be_created()
   {
       $response = $this->postJson("/api/products", [
           "name" => "Product #1",
           "unit_id" => $unitId = factory(Unit::class)->create()->id,
           "quantity" => 10,
           "minimum_quantity" => 5,
           "comment" => "This is used for blah and blah"
       ]);

       $response->assertStatus(201);

       $this->assertDatabaseHas("products", [
           "business_id" => $this->user->business_id,
           "name" => "Product #1",
           "unit_id" => $unitId,
           "quantity" => 10,
           "minimum_quantity" => 5,
           "comment" => "This is used for blah and blah"
       ]);
   }
}
