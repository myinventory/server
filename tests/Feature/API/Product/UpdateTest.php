<?php

namespace Tests\Feature\API\Product;

use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCaseUser;

class UpdateTest extends TestCaseUser
{
    /** @test */
    public function it_can_be_updated()
    {
        $product = factory(Product::class)->create();

        $response = $this->patchJson("/api/products/$product->id", array_merge($product->toArray(),
            [
                "name" => "Chocolate"
            ]
        ));

        $response->assertStatus(200);

        $this->assertDatabaseHas("products", [
            "id" => $product->id,
            "name" => "Chocolate"
        ]);
    }
}
