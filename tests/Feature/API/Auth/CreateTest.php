<?php

namespace Tests\Feature\API\Auth;

use App\Business;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateTest extends TestCase
{
    /** @test */
    public function a_guest_can_create_a_user()
    {
        \Artisan::call('passport:install');

        $this->withoutExceptionHandling();
        // be guest

        $response = $this->postJson("/api/auth/users", [
            "email" => "admin@kaoz.space",
            "password" => "timetravel"
        ]);

        // submit details

        $response->assertStatus(201);

        // create new business

        $this->assertCount(1, Business::all());

        // ensure tenant was created

        $this->assertDatabaseHas("users", [
            "email" => "admin@kaoz.space"
        ]);
    }
}
