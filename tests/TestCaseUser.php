<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Passport\Passport;

abstract class TestCaseUser extends BaseTestCase
{
    use CreatesApplication;

    use RefreshDatabase;

    public $user = null;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        Passport::actingAs(
            $this->user
        );
    }
}
