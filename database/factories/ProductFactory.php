<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\Unit;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        "name" => $faker->name,
        "unit_id" => factory(Unit::class),
        "quantity" => $quantity = $faker->numberBetween(0,10),
        "minimum_quantity" => $faker->numberBetween(0, $quantity),
        "comment" => $faker->paragraph
    ];
});
