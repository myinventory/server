<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Taker;
use Faker\Generator as Faker;

$factory->define(Taker::class, function (Faker $faker) {
    return [
        "name" => $faker->name
    ];
});
