<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\History;
use Faker\Generator as Faker;

$factory->define(History::class, function (Faker $faker) {
    return [
        "product_id" => factory(\App\Product::class),
        "taker_id" => factory(\App\Taker::class),
        "action" => "ADD",
        "quantity" => $quantity = $faker->numberBetween(1,10)
    ];
});
