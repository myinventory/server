<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriesTable extends Migration
{
    static $ADD = "ADD";
    static $TAKE = "TAKE";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger("quantity");

            $table->enum("action", [
                static::$ADD,
                static::$TAKE
            ]);

            $table->unsignedBigInteger("business_id");
            $table->foreign("business_id")->references("id")->on("businesses");

            $table->unsignedBigInteger("taker_id")->nullable();
            $table->foreign("taker_id")->references("id")->on("takers")->onDelete('set null');

            $table->unsignedBigInteger("product_id");
            $table->foreign("product_id")->references("id")->on("products");

            $table->text("comment")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
